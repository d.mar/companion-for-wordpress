# Companion for Wordpress 

(Work in progress).

### Prerequisites.

* PHP 7.2+.

### Installing & Uninstalling.

Like any other Wordpress plugin.

## Authors

* **David Martín** - *Developer* - [dmar.dev](https://gitlab.com/dmar.dev)

## License

This project is licensed under the GPL2 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
