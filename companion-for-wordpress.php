<?php defined( 'ABSPATH' ) OR exit;
/*
Plugin Name: Companion for Wordpress
Plugin URI: https://gitlab.com/dmar.dev/companion-for-wordpress
Description: Common tasks for Wordpress.
License: GPL2
Text Domain: companion-for-wordpress
Domain Path: /languages
Requires PHP: 7.2

Author: David Martí­n
Version: 0.1.0
Author URI: https://dmar.dev
*/

// ----------------------------------------------------------------------------
// Plugin Bootstrap.
// ----------------------------------------------------------------------------
// TODO:
// Check for (future) Unit testing.
// Check for hooks in Multisite (it seems buggy).
// ----------------------------------------------------------------------------

// Define min PHP version to work with.
// ----------------------------------------------------------------------------

define( 'CFW_MIN_PHP_VER', '5.6' );

// Define plugin version.
// ----------------------------------------------------------------------------

define( 'CFW_VER', '0.1.0' );

// Define Classes path.
// ----------------------------------------------------------------------------

define( 'CFW_PATH', plugin_dir_path( __FILE__ ) );

// Require plugin app.
// ----------------------------------------------------------------------------

require_once CFW_PATH . 'plugin/App.php';	

// Activation Hook.
// ----------------------------------------------------------------------------

register_activation_hook( 
	__FILE__,
	array( '\plugin\App', 'onActivation' )
);

// Deactivation Hook.
// ----------------------------------------------------------------------------

register_deactivation_hook( 
	__FILE__,
	array( '\plugin\App', 'onDeactivation' )
);

// Uninstall Hook.
// ----------------------------------------------------------------------------

register_uninstall_hook( 
	__FILE__,
	array( '\plugin\App', 'onUninstall' )
);

// Fire it.
// ----------------------------------------------------------------------------

$plugin = new \plugin\App();

$plugin->init();

// ----------------------------------------------------------------------------