<?php

namespace plugin;

use \plugin\classes\Admin as Admin;

// ----------------------------------------------------------------------------

defined( 'ABSPATH' ) OR exit;

/**
* Main Plugin class.
* 
*/

class App
{	

	private $admin;
	private $options;

	public function __construct()
    {       	
    	$this->classLoader();
    	$this->options = get_option( 'cfw_options' );
    }	
    /**
    * On Activate Hook.
    *
    * 
    */
    public static function onActivation()
    {
    	
    }
    /**
    * On Deactivate Hook.
    *
    * 
    */
    public static function onDeactivation()
    {

    }
    /**
    * On Uninstall Hook.
    *
    * 
    */
    public static function onUninstall()
    {

    }
    private function classLoader()
    {    	
    	spl_autoload_register( function ($class) {

			$file = CFW_PATH . preg_replace('#\\\|_(?!.+\\\)#','/', $class) . '.php';
			
			if ( stream_resolve_include_path($file) )
			{				
				require $file;
			}	
		});
    }
    private function admin()
    {
    	$this->admin = new Admin();
    }    
    public function init()
    {
    	add_action( 
    		'admin_init',
    		array(
    			$this, 'run'
    		)
    	);

    	// Clean header.
    	// --------------------------------------------------------------------

    	//var_dump($options); exit();

        if( isset( $this->options['disable_features_pingbacks'] ) )
        {
            if( $this->options['disable_features_pingbacks'] == 1 )   
            {
                add_filter( 'pre_option_default_ping_status', '__return_zero' );
                add_filter( 'pre_option_default_pingback_flag', '__return_zero' );
                add_filter(
                    'wp_headers',
                    array( $this, 'dmar_remove_pingback_header' ),
                    9999,
                    2
                );
                add_filter(
                    'xmlrpc_methods',
                    array ( $this, 'dmar_disable_pingbacks'),
                    9999,
                    2
                );
            }
        }
        if( isset( $this->options['disable_features_xmlrpc_api'] ) )
        {
            if( $this->options['disable_features_xmlrpc_api'] == 1 )
            {
                add_filter('xmlrpc_enabled', '__return_false');
            }
        }
        if( isset( $this->options['disable_features_json_api']) )
        {
            if( $this->options['disable_features_json_api'] == 1 )
            {
                remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
                remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
            }
        }
        if( isset( $this->options['disable_features_editor']) )
        {
            if( $this->options['disable_features_editor'] == 1 )
            {
                define( 'DISALLOW_FILE_EDIT', true );
            }
        }
    	if( isset( $this->options['header_version']) )
    	{
    		if( $this->options['header_version'] == 1 )
    		{    			
    			remove_action('wp_head', 'wp_generator');
    			add_filter('the_generator', '__return_false');  			    			
    			add_filter( 
					'style_loader_src',
					array( $this, 'dmar_remove_ver_from_assets'),
					9999,
					2
				);
				add_filter( 
					'script_loader_src',
					array( $this, 'dmar_remove_ver_from_assets'),
					9999,
					2
				);
    		}    		

    	}
    	if( isset( $this->options['header_emoji']) )
    	{
    		if( $this->options['header_emoji'] == 1 )
    		{    			
    			remove_action( 'wp_head' , 'print_emoji_detection_script', 7);
				remove_action( 'wp_print_styles' , 'print_emoji_styles');
				remove_action( 'admin_print_scripts' , 'print_emoji_detection_script' );
				remove_action( 'admin_print_styles' , 'print_emoji_styles' );
				add_filter( 'emoji_svg_url', '__return_false' );
    		}    		

    	}
    	if( isset($this->options['header_rsd']) )
    	{
    		if( $this->options['header_rsd'] == 1 )
    		{
    			remove_action( 'wp_head' , 'rsd_link' );
    		}
    	}
    	if( is_admin() )
    	{
    		$this->admin();
    	}     	
    		
    }
    public function run()
    {
    	$creds = '';

    	if ( ! WP_Filesystem($creds) )
    	{
			request_filesystem_credentials($url, '', true, false, null);
			return;
		}

		global $wp_filesystem;
    	  
    	$lines 		= array();  
    	$htaccess 	= ABSPATH . ".htaccess";

    	// Core files access and permissions.
    	// --------------------------------------------------------------------

    	//var_dump($options); exit();

    	if( isset( $this->options['file_config'] ) )
    	{   	
    	 	if( $this->options['file_config'] == 1 )
	    	{    		
	    		$lines[] =  "<Files wp-config.php>\norder allow,deny\ndeny from all\n</Files>";				
	    	}
	    	else
	    	{
	    		$lines[] = "";				
	    	}
	    }
	    if( isset( $this->options['file_xmlrpc'] ) )
	    {	    	
	    	if( $this->options['file_xmlrpc'] == 1 )
	    	{
	    		$lines[] =  "<Files xmlrpc.php>\norder allow,deny\ndeny from all\n</Files>";			
	    	}
	    	else
	    	{	    		
				$lines[] = "";				
	    	}
	    }
	    if( isset( $this->options['file_readme']) )
	    {
	    	if( $this->options['file_readme'] == 1 )
	    	{
	    		$lines[] =  "<Files readme.html>\norder allow,deny\ndeny from all\n</Files>";			
	    	}
	    	else
	    	{	    		
				$lines[] = "";				
	    	}
	    }
	    if( isset( $this->options['file_per']) )
	    {
	    	if( $this->options['file_per'] == 1)
	    	{
	    		$wp_filesystem->chmod( ABSPATH . "wp-config.php", 0600, false);
	    		$wp_filesystem->chmod( ABSPATH . "readme.html", 0200, false);
				$wp_filesystem->chmod( ABSPATH . "xmlrpc.php", 0644, false);
	    	}
	    	else
	    	{
	    		$wp_filesystem->chmod( ABSPATH . "wp-config.php", 0644, false);
	    		$wp_filesystem->chmod( ABSPATH . "readme.html", 0644, false);
				$wp_filesystem->chmod( ABSPATH . "xmlrpc.php", 0644, false);
	    	}
	    }    		    	

	    insert_with_markers($htaccess, "companion-for-wordpress", $lines);					    	

    }    
    public function dmar_remove_ver_from_assets( $src )
    {
    	if (strpos($src, 'ver='))
		{
			$src = remove_query_arg('ver', $src);
		}

		return $src;
    }
    public function dmar_remove_pingback_header( $headers )
	{
		unset($headers['X-Pingback']);

	  	return $headers;
	}
	public function dmar_disable_pingbacks( $methods )
	{
	  unset($methods['pingback.ping']);
	  unset($methods['pingback.extensions.getPingbacks']);

	  return $methods;
	}		
	public function test(){
		remove_action( 
    				'wp_head',
    				'wp_generator'
    			);
    			add_filter( 
    				'the_generator',
    				'__return_null'
    			);
	}
}