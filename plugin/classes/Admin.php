<?php

namespace plugin\classes;

use plugin\classes\View as View;

// ----------------------------------------------------------------------------

defined( 'ABSPATH' ) OR exit;

/**
* Admin class.
*
*/

class Admin
{
	private $view;

	public function __construct()
    {   
        $this->view = new View();     
        $this->loadScripts();
        $this->loadStyles();

    	add_action(
    		'admin_menu',
    		array( $this, 'menu' )
    	);
        add_action( 
            'admin_init',
            array( $this, 'registerSettings')
        );                
    }
    private function loadScripts()
    {
        add_action( 'admin_enqueue_scripts', function(){
            wp_enqueue_script( 
                'cfw-admin',
                plugins_url( 'companion-for-wordpress/plugin/templates/admin/assets/js/cfw-admin.js' ),
                array('jquery'),
                null, 
                true
            );    
        });
    }
    private function loadStyles()
    {
        add_action( 'admin_enqueue_scripts', function(){
            wp_enqueue_style( 
                'cfw-admin',
                plugins_url( 'companion-for-wordpress/plugin/templates/admin/assets/css/cfw-admin.css' ),
                array(),
                null, 
                'all'
            );    
        });
    }
    public function registerSettings()
    {
        $defaults = array(
            'type'      => 'int',
            'default'   => '0'
        );

        add_settings_section(
            'cfw_disable_features',
            '',
            function() {},
            'cfw-disable-features'
        );
        
        add_settings_section(
            'cfw_files',
            '',
            function() {},
            'cfw-files'
        );
        // Header
        add_settings_section(
            'cfw_header',
            '',
            function() {},
            'cfw-header'
        );

        // Disble Pingbacks and Trackbacks.
        // --------------------------------------------------------------------

        add_settings_field(
            'cfw_disable_features_pingbacks',
            '<i>Pingbacks and Trackbacks</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );                
                ?>
                <div id="header-version">                        
                        <label for="cfw_disable_features_pingbacks">                                                     
                            <input 
                                id="cfw_disable_features_pingbacks"
                                name="cfw_options[disable_features_pingbacks]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['disable_features_pingbacks']) )
                                    {
                                        checked( $options['disable_features_pingbacks'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Disable Wordpress Pingbacks and Trackbacks.', 'companion-for-wordpress'); ?>
                        </label>
                    </div>
                <?php
            },
            'cfw-disable-features',
            'cfw_disable_features',
            [
                'label_for'         => 'disable_features_pingbacks',
                'class'             => 'cfw_row',
                'cfw_custom_data'   => 'custom',
            ]
        );

        // Disble Xml-rpc API.
        // --------------------------------------------------------------------

        add_settings_field(
            'cfw_disable_features_xmlrpc_api',
            '<i>Xml RPC API</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );                
                ?>
                <div id="header-version">                        
                        <label for="cfw_disable_features_xmlrpc_api">                                                     
                            <input 
                                id="cfw_disable_features_xmlrpc_api"
                                name="cfw_options[disable_features_xmlrpc_api]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['disable_features_xmlrpc_api']) )
                                    {
                                        checked( $options['disable_features_xmlrpc_api'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Disable Wordpress XML RPC API.', 'companion-for-wordpress'); ?>
                        </label>
                    </div>
                <?php
            },
            'cfw-disable-features',
            'cfw_disable_features',
            [
                'label_for'         => 'disable_features_xmlrpc_api',
                'class'             => 'cfw_row',
                'cfw_custom_data'   => 'custom',
            ]
        );

        // Disble JSON API.
        // --------------------------------------------------------------------

        add_settings_field(
            'cfw_disable_features_json_api',
            '<i>Json API</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );                
                ?>
                <div id="header-version">                        
                        <label for="cfw_disable_features_json_api">                                                     
                            <input 
                                id="cfw_disable_features_json_api"
                                name="cfw_options[disable_features_json_api]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['disable_features_json_api']) )
                                    {
                                        checked( $options['disable_features_json_api'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Disable Wordpress Json API.', 'companion-for-wordpress'); ?>
                        </label>
                    </div>
                <?php
            },
            'cfw-disable-features',
            'cfw_disable_features',
            [
                'label_for'         => 'disable_features_json_api',
                'class'             => 'cfw_row',
                'cfw_custom_data'   => 'custom',
            ]
        );

        // Disble Wordpress file editor.
        // --------------------------------------------------------------------

        add_settings_field(
            'cfw_disable_features_editor',
            '<i>Wordpress files editor</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );                
                ?>
                <div id="header-version">                        
                        <label for="cfw_disable_features_editor">                                                     
                            <input 
                                id="cfw_disable_features_editor"
                                name="cfw_options[disable_features_editor]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['disable_features_editor']) )
                                    {
                                        checked( $options['disable_features_editor'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Disable Wordpress file editor.', 'companion-for-wordpress'); ?>
                        </label>
                    </div>
                <?php
            },
            'cfw-disable-features',
            'cfw_disable_features',
            [
                'label_for'         => 'disable_features_editor',
                'class'             => 'cfw_row',
                'cfw_custom_data'   => 'custom',
            ]
        );

        // Header emoji
        // --------------------------------------------------------------------

        add_settings_field(
            'cfw_disable_features_emoji',
            '<i>Emoji</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );                
                ?>
                <div id="header-version">
                        
                        <label for="cfw_header_emoji">                         
                            
                            <input 
                                id="cfw_header_emoji"
                                name="cfw_options[disable_features_emoji]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['disable_features_emoji']) )
                                    {
                                        checked( $options['disable_features_emoji'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Remove emoji code from HEAD.', 'companion-for-wordpress'); ?>
                        </label>

                    </div>
                <?php
            },
            'cfw-disable-features',
            'cfw_disable_features',
            [
                'label_for'                     => 'disable_features_emoji',
                'class'                         => 'cfw_row',
                'cfw_custom_data'               => 'custom',
            ]
        );

         // Header RSD.
        // --------------------------------------------------------------------
        
        add_settings_field(
            'cfw_disable_features_rsd',
            '<i>RSD</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );                
                ?>
                <div id="header-version">                        
                        <label for="cfw_header_rsd">                                                     
                            <input 
                                id="cfw_header_rsd"
                                name="cfw_options[disable_features_rsd]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['disable_features_rsd']) )
                                    {
                                        checked( $options['disable_features_rsd'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Remove RSD code from HEAD.', 'companion-for-wordpress'); ?>
                        </label>

                    </div>
                <?php
            },
            'cfw-disable-features',
            'cfw_disable_features',
            [
                'label_for'         => 'cfw_disable_features_rsd',
                'class'             => 'cfw_row',
                'cfw_custom_data'   => 'custom',
            ]
        );

        // Files permissions.
        //---------------------------------------------------------------------

        add_settings_field(
            'cfw_files_per',
            '<i>Permissions</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );
                
                ?>
                <div id="files-permissions">
                        
                        <label for="cfw_files_per">                                                     
                            <input 
                                id="cfw_files_per"
                                name="cfw_options[file_per]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['file_per']) )
                                    {
                                        checked( $options['file_per'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Change permissions to certain core files.', 'companion-for-wordpress'); ?>
                        </label>

                    </div>
                <?php
            },
            'cfw-files',
            'cfw_files',
            [
                'label_for'                     => 'cfw_files_per',
                'class'                         => 'cfw_row',
                'cfw_custom_data'               => 'custom',
            ]
        );

        // wp-config.php
        //---------------------------------------------------------------------

        add_settings_field(
            'cfw_files_config',
            '<i>wp-config.php</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );

                ?>
                <div id="files-config">
                        
                        <label for="cfw_files_config">                                                     
                            <input 
                                id="cfw_files_config"
                                name="cfw_options[file_config]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['file_config']) )
                                    {
                                        checked( $options['file_config'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Protect <i>wp-config.php</i> file through htaccess.', 'companion-for-wordpress'); ?>
                        </label>

                    </div>
                <?php
            },
            'cfw-files',
            'cfw_files',
            [
                'label_for'                     => 'cfw_files_config',
                'class'                         => 'cfw_row',
                'cfw_custom_data'               => 'custom',
            ]
        );


        // xmlrpc.php
        //---------------------------------------------------------------------

        add_settings_field(
            'cfw_files_xmlrpc',
            '<i>xmlrpc.php</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );

                ?>
                <div id="files-xmlrpc">
                        
                        <label for="cfw_files_xmlrpc">                                                     
                            <input 
                                id="cfw_files_xmlrpc"
                                name="cfw_options[file_xmlrpc]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset($options['file_xmlrpc']) )
                                    {
                                        checked( $options['file_xmlrpc'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Protect <i>xmlrpc.php</i> file through htaccess.', 'companion-for-wordpress'); ?>
                        </label>

                    </div>
                <?php
            },
            'cfw-files',
            'cfw_files',
            [
                'label_for'                     => 'cfw_files_xmlrpc',
                'class'                         => 'cfw_row',
                'cfw_custom_data'               => 'custom',
            ]
        );

        // readme.html leeme.html
        //---------------------------------------------------------------------
        
        add_settings_field(
            'cfw_files_readme',
            '<i>readme.html</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );
                //var_dump($options['file_config']); 
                ?>
                <div id="files-readme">
                        
                        <label for="cfw_files_readme">                         
                            <?php //var_dump(checked( $options['file_config'], 1 )); ?>
                            <input 
                                id="cfw_files_readme"
                                name="cfw_options[file_readme]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset($options['file_readme']) )
                                    {
                                        checked( $options['file_readme'], '1' );
                                    }                                   
                                ?> 
                            />
                            <?php _e('Protect <i>readme.html</i> file through htaccess.', 'cfw'); ?>
                        </label>

                    </div>
                <?php
            },
            'cfw-files',
            'cfw_files',
            [
                'label_for'                     => 'cfw_files_readme',
                'class'                         => 'cfw_row',
                'cfw_custom_data'  => 'custom',
            ]
        );

        // Header version.
        //---------------------------------------------------------------------

        add_settings_field(
            'cfw_header_version',
            '<i>Wordpress version</i>',
            function( $args )
            { 
                $options = get_option( 'cfw_options' );                
                ?>
                <div id="header-version">
                        
                        <label for="cfw_header_version">                         
                            
                            <input 
                                id="cfw_header_version"
                                name="cfw_options[header_version]"
                                type="checkbox" 
                                value="1" 
                                <?php 
                                    if( isset( $options['header_version']) )
                                    {
                                        checked( $options['header_version'], '1' );
                                    }                                    
                                ?> 
                            />
                            <?php _e('Hide the Wordpress version from HEAD and enqueued files (query strings).', 'cfw'); ?>
                        </label>

                    </div>
                <?php
            },
            'cfw-header',
            'cfw_header',
            [
                'label_for'                     => 'cfw_header_version',
                'class'                         => 'cfw_row',
                'cfw_custom_data'  => 'custom',
            ]
        );       

        register_setting( 
            'cfw-options',
            'cfw_options',
            array( $this, 'plugin_options_validate' )
        );

    }
    public function plugin_options_validate( $input )
    {        
        if( ! isset($input['file_config']) )
        {
            $input['file_config'] = 0;
        }
         if( ! isset($input['file_xmlrpc']) )
        {
            $input['file_xmlrpc'] = 0;
        }
        if( ! isset($input['file_readme']) )
        {
            $input['file_readme'] = 0;
        }
        if( ! isset($input['file_per']) )
        {
            $input['file_per'] = 0;
        }
        if( ! isset( $input['header_version'] ) )
        {
            $input['header_version'] = 0;
        }
        if( ! isset( $input['disable_features_emoji'] ) )
        {
            $input['disable_features_emoji'] = 0;
        }
        if( ! isset( $input['disable_features_rsd'] ) )
        {
            $input['disable_features_rsd'] = 0;
        }
        if( ! isset( $input['disable_features_editor'] ) )
        {
            $input['disable_features_editor'] = 0;
        }
        if( ! isset( $input['disable_features_json_api'] ) )
        {
            $input['disable_features_json_api'] = 0;
        }
        if( ! isset( $input['disable_features_xmlrpc_api'] ) )
        {
            $input['disable_features_xmlrpc_api'] = 0;
        }
        if( ! isset( $input['disable_features_pingbacks'] ) )
        {
            $input['disable_features_pingbacks'] = 0;
        }
        
        $options = get_option('cfw_options');
        $options['file_config']                 = trim($input['file_config']);
        $options['file_xmlrpc']                 = trim($input['file_xmlrpc']);
        $options['file_readme']                 = trim($input['file_readme']);
        $options['file_per']                    = trim($input['file_per']);
        $options['header_version']              = trim($input['header_version']);
        $options['disable_features_emoji']      = trim($input['disable_features_emoji']);
        $options['disable_features_rsd']        = trim($input['disable_features_rsd']);
        $options['disable_features_editor']     = trim($input['disable_features_editor']);
        $options['disable_features_json_api']   = trim($input['disable_features_json_api']);
        $options['disable_features_xmlrpc_api'] = trim($input['disable_features_xmlrpc_api']);
        $options['disable_features_pingbacks']  = trim($input['disable_features_pingbacks']);
        
        return $options;
    }
    public function menu()
    {   	
    	add_menu_page( 
	    	'WP Security Info',
	    	'Companion for Wordpress',
	    	'manage_options',
	    	'cfw-settings',
	    	array( $this, 'settingsPage' ),
	    	plugins_url( 'companion-for-wordpress/assets/img/icon.png' )
	    );    		
    }    
    public function settingsPage()
    {
    	$this->view->setTemplateName('settings');
    	$this->view->loadAdminTemplate();
    }
}