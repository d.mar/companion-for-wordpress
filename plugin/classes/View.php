<?php

namespace plugin\classes;

/**
 * 
 */
class View
{
	private $pluginData;

	private $folder;
	private $adminFolder;

	private $templateName;
	private $templateHeader;
	private $templateFooter;
	
	public function __construct()
	{
		$this->pluginData 		= $this->setPluginData();
		$this->folder 			= CFW_PATH . 'plugin/templates';
		$this->adminFolder 		= CFW_PATH . 'plugin/templates/admin/';
		$this->templateName 	= 'info';
		$this->templateHeader 	= 'header';
		$this->templateFooter 	= 'footer';
	}
	public function setTemplateName( $name )
	{
		$this->templateName = $name;
	}
	public function getTemplateName()
	{
		return $this->templateName;
	}
	public function loadTemplate()
	{
		$file = $this->folder . $this->templateName . '.php';

		if( file_exists( $file ) )
		{
			require_once $file;
			return;
		}

		return false;
	}
	public function loadAdminTemplate()
	{
		$file = $this->adminFolder . $this->templateName . '.php';		

		if( file_exists( $file ) )
		{
			require_once $this->adminFolder . 'includes/' . $this->templateHeader . '.php';
			require_once $file;
			require_once $this->adminFolder . 'includes/' . $this->templateFooter . '.php';
			return;
		}

		return false;
	}
	private function setPluginData()
	{
		if ( is_admin() )
		{
			if( ! function_exists( 'get_plugin_data' ) )
			{
				require_once ABSPATH . 'wp-admin/includes/plugin.php';
			}
			
			return get_plugin_data( CFW_PATH . 'companion-for-wordpress.php' , false, true );
		}

		return false;			
	}
	public function getPluginData()
	{
		return $this->pluginData;
	}
}