jQuery(document).ready( function( $ )
{
	$('.nav-tab-wrapper a').click(function( event )
	{
		event.preventDefault();
		
		var context = $(this).parents('.nav-tab-wrapper').first().parent();
		$('.nav-tab a', context).removeClass('nav-tab-active');
		$(this).parents('a').first().addClass('nav-tab-active');
		$('.tab-panel', context).hide();
		$( $(this).attr('href'), context ).show();
	});		
	$('.nav-tab').each(function()
	{
		if ( $('nav-tab-active', this).length )
		{				
			$('.nav-tab-active', this).click();
		}
		else
		{
			$('a', this).first().click();	
		} 
	});
});