<div class="wrap">
	
	<header id="companion-for-wordpress-header">
			
		<h1 class="wp-heading-inline">
			<?php echo $this->getPluginData()['Name']; ?>
			<code>
				v. <?php echo $this->getPluginData()['Version']; ?>
			</code>
		</h1>		
		
		<h2>
			<?php echo $this->getPluginData()['Description']; ?> <br />				
		</h2>

		<p>
			<small>
				<?php _e('Plugin developed by', 'companion-for-wordpress'); ?>
				<a href="<?php echo $this->getPluginData()['AuthorURI']; ?>" target="_blank">
					<b><?php echo $this->getPluginData()['Author']; ?></b>
				</a> · 
				<?php _e('Hosted in', 'companion-for-wordpress'); ?>
				<a href="<?php echo $this->getPluginData()['PluginURI']; ?>" target="_blank">
					<b>Gitlab</b>
				</a> · 
				<a href="<?php echo $this->getPluginData()['PluginURI']; ?>/blob/master/LICENSE" target="_blank">
					<b>GPL2</b>
				</a>
			</small>
		</p>
		
		<hr />			

	</header><!-- #companion-for-wordpress-header -->

	<div id="col-container">
		
	