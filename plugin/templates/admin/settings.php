<?php 
	// Check user rol.
	// ------------------------------------------------------------------------

	if ( ! current_user_can( 'manage_options' ) )
	{ 
		return;
	}

	// Check if settigs came updated and show message.
	// ------------------------------------------------------------------------

	if ( isset( $_GET['settings-updated'] ) )
	{ 
 		add_settings_error( 
 			'cfw_messages',
 			'cfw_message',
 			__( 'Settings Saved', 'cfw' ),
 			'updated'
 		);
 	} 
 
 	settings_errors( 'cfw_messages' );

 	// Check active tab.
	// ------------------------------------------------------------------------

	$active_tab = 'disable-features';

	if( isset($_GET['tab']) )
	{
		$active_tab = filter_var( $_GET['tab'], FILTER_SANITIZE_STRING);
	}
?>
<div id="col-right">

	<div class="col-wrap">
		
		<h1>TEXT</h1>

		<div class="inside">
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae nibh magna. Morbi lacus risus, sodales at felis vitae, tempus porttitor ante. Vestibulum nec dapibus massa. Donec tempor orci nec erat hendrerit pretium eget et risus. Pellentesque tempus tortor at libero ornare iaculis. Morbi quis ante ligula. Aliquam finibus magna augue, sed accumsan sapien commodo et. In sit amet semper nulla. In metus ligula, luctus nec dolor vel, molestie egestas urna. Suspendisse pharetra lobortis risus, at luctus nisi aliquam eget. Nunc tincidunt ante libero, eu malesuada risus ultricies eget. Duis vehicula nulla vitae nisi sodales, vitae aliquam purus varius. 
		</div>

	</div><!-- .col-wrap -->
	
</div><!-- .col-right -->

<div id="col-left">

	<div class="col-wrap">		

		<section class="inside">			
			
			<h2 class="nav-tab-wrapper">
				<a 
					href="#disable-features"
					class="nav-tab <?php echo $active_tab == 'disable-features' ? 'nav-tab-active' : '';?>">
					<?php _e('Disable Wordpress features', 'companion-for-wordpress'); ?>
				</a>
				<a 
					href="#security"
					class="nav-tab <?php echo $active_tab == 'security' ? 'nav-tab-active' : '';?>">
					<?php _e('Security', 'companion-for-wordpress'); ?>
				</a>			
				<a 
					href="#clean-header" 
					class="nav-tab <?php echo $active_tab == 'clean-header' ? 'nav-tab-active' : '';?>">
					<?php _e('Clean Header', 'companion-for-wordpress'); ?>
				</a>				
			</h2>			

			<form method="post" action="options.php">

				<?php settings_fields( 'cfw-options' ); ?>

				<div id="disable-features" class="tab-panel">
					<?php do_settings_sections( 'cfw-disable-features' ); ?>
				</div>
				<div id="security" class="tab-panel hide">					
					<?php do_settings_sections( 'cfw-files' ); ?>				
				</div>
				<div id="clean-header" class="tab-panel hide">					
					<?php do_settings_sections( 'cfw-header' ); ?>
				</div>

				<hr />

				<?php submit_button(); ?>

			</form>

		</section><!--.inside -->	

	</div><!-- .col-wrap -->
	
</div><!-- .col-right -->